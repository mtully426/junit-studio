import org.junit.Test;
import static org.junit.Assert.*;

public class RomanNumberalTest {

    @Test
    public void check25(){
        assertEquals("XXV", RomanNumeral.fromInt(25));
    }

    @Test
    public void check115(){
        assertEquals("CXV", RomanNumeral.fromInt(115));
    }

    @Test
    public void check2014(){
        assertEquals("MMXIV", RomanNumeral.fromInt(2014));
    }

    @Test
    public void check2009(){
        assertEquals("MMIX", RomanNumeral.fromInt(2009));
    }

    @Test
    public void check1998(){
        assertEquals("MCMXCVIII", RomanNumeral.fromInt(1998));
    }

    @Test
    public void check999(){
        assertEquals("CMXCIX", RomanNumeral.fromInt(999));
    }
}
