import org.junit.Test;
import static org.junit.Assert.*;

public class BinarySearchTest {

    @Test
    public void fiveNumberSearch(){
        int[] numbers = {1, 6, 7, 8, 9};
        assertEquals(0, BinarySearch.binarySearch(numbers, 1));
    }

    @Test
    public void sixNumberSearch(){
        int[] numbers = {1, 3, 6, 7, 8, 9};
        assertEquals(2, BinarySearch.binarySearch(numbers, 6));
    }

    @Test
    public void sevenNumberSearch(){
        int[] numbers = {1, 2, 4, 6, 7, 8, 9};
        assertEquals(0, BinarySearch.binarySearch(numbers, 1));
    }

    @Test
    public void fiveNumberSearchNotFound(){
        int[] numbers = {1, 6, 7, 8, 9};
        assertEquals(-1, BinarySearch.binarySearch(numbers, 5));
    }

    @Test
    public void sixNumberSearchNotFound(){
        int[] numbers = {1, 3, 6, 7, 8, 9};
        assertEquals(-1, BinarySearch.binarySearch(numbers, 5));
    }

    @Test
    public void sevenNumberSearchNotFound(){
        int[] numbers = {1, 2, 4, 6, 7, 8, 9};
        assertEquals(-1, BinarySearch.binarySearch(numbers, 3));
    }

    @Test
    public void eightNumberSearchNotFound(){
        int[] numbers = {1, 2, 4, 5, 6, 7, 8, 9};
        assertEquals(-1, BinarySearch.binarySearch(numbers, 3));
    }

    @Test
    public void eightNumberSearch(){
        int[] numbers = {1, 2, 4, 5, 6, 7, 8, 9};
        assertEquals(7, BinarySearch.binarySearch(numbers, 9));
    }

    @Test
    public void sevenNumberSearchTwoDigit(){
        int[] numbers = {12, 24, 46, 64, 77, 82, 98};
        assertEquals(2, BinarySearch.binarySearch(numbers, 46));
    }

}
