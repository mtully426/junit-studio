import org.junit.Test;
import static org.junit.Assert.*;


public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void checkJustBrackets(){
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("[]"));
    }

    @Test
    public void checkBracketedWord(){
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("[asdfa]"));
    }

    @Test
    public void checkThreeBrackets(){
        assertEquals(true, BalancedBrackets.hasBalancedBrackets("[asdfa[]asda[asda]]"));
    }

    @Test
    public void failThreeBrackets(){
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("[as]dfa[]asda[asda]]"));
    }

    @Test
    public void checkThreeBracketsOutofOrder(){
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("]asdfa[[asda[asda]]"));
    }

    @Test
    public void checkFourBracketsOutofOrder(){
        assertEquals(false, BalancedBrackets.hasBalancedBrackets("]asdf][a[[asda[asda]]"));
    }

}
