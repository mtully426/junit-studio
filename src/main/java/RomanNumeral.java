
public class RomanNumeral {

    enum Numeral {
        I(1), IV(4), V(5), IX(9), X(10), IL(40), L(50), XC(90), C(100), CD(400), D(500), CM(900), M(1000);
        int value;

        Numeral(int value) {
            this.value = value;
        }
    }

    /**
     * Convert positive integers to roman numerals
     *
     * Examples: 4 -> IV, 51 -> LI, 999 -> CMXCIX
     *
     * @param n - positive integer
     * @return Roman numeral string version of integer
     */
    public static String fromInt (int n) {

        if(n <= 0) {
            throw new IllegalArgumentException();
        }

        StringBuilder buf = new StringBuilder();

        final Numeral[] values = Numeral.values();
        for (int i = values.length - 1; i >= 0; i--) {
           /*
            while (n >= values[i].value) {
                if(Integer.toString(n).toCharArray()[0] == '4') {
                    buf.append(values[i]);
                    buf.append(values[i + 1]);
                    char[] numArray = Integer.toString(n).toCharArray();
                    n -= (4 * (numArray.length * 10));
                }
                else if (Integer.toString(n).toCharArray()[0] == '9'){
                    buf.append(values[i - 1]);
                    buf.append(values[i + 1]);
                    char[] numArray = Integer.toString(n).toCharArray();
                    n -= (9 * ((numArray.length * 10)*numArray.length));
                }
                else {
                    buf.append(values[i]);
                    n -= values[i].value;
                }
*/            while (n >= values[i].value) {
            buf.append(values[i]);
            n -= values[i].value;
            }
        }
        return buf.toString();
    }

}
